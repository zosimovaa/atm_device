from atm import ATM
from atm.cfg_device import cfg_ch, cfg_cr, cfg_down

atm_cfg = {
    "ID": 0,
    "devices": [cfg_ch, cfg_cr, cfg_down]
}

srv_cfg = {
    "FLM": 3,
    "SLM": 8,
    "INk": 12
}
atm = ATM(atm_cfg, srv_cfg)
atm.get_state()

