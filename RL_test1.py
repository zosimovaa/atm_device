import gym
from gym import spaces


class CustomEnv(gym.Env):
    """Custom Environment that follows gym interface"""
    metadata = {'render.modes': ['human']}
    N_DISCRETE_ACTIONS = 3  # Размерность action_space

    def __init__(self):
        super(CustomEnv, self).__init__()
        # action_space -  дискретный тип
        self.action_space = spaces.Discrete(self.N_DISCRETE_ACTIONS)

        # observation_space: мультидисктет
        self.observation_space = spaces.Box(low=0, high=255,
                                            shape=(HEIGHT, WIDTH, N_CHANNELS), dtype=np.uint8)

    def step(self, action):
        ...
        return observation, reward, done, info

    def reset(self):
        ...
        return observation  # reward, done, info can't be included

    def render(self, mode='human'):
        ...

    def close(self):
        ...
