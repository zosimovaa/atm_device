import numpy as np
import logging

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(name)s [%(levelname)s]: %(message)s')

class ATM_DEVICE_ERROR(Exception):
    def __init__(self, alias, code, err):
        self.alias = alias
        self.code = code
        self.err = err

class DEVICE:
    def __init__(self, config):
        self.name = config["name"]
        self.alias = config["alias"]
        self.state = 0
        self.available_resource = 0

        self.error_codes = {}
        for key, value in config["errors"].items():
            self.error_codes[key] = value
            self.error_codes[key]["available_resource"] = value["resource"]

        self.update_resource()
        self.logger = logging.getLogger('dev_{}'.format(self.alias))
        self.logger.debug("device initialized")
        return

    def update_resource(self):
        """
        Обновление информации о доступном ресурсе
        :return: доступный ресурс
        """
        resources = [value["available_resource"] for key, value in self.error_codes.items()]
        self.available_resource = min(resources)
        return


    def utilize_resource(self, resource=1):
        """
        Утилизирует ресурс транзакций, возвращает статус
        :return:
        """
        if resource < 1:
            resource = self.available_resource
        else:
            resource = min(resource, self.available_resource)

        for key, value in self.error_codes.items():
            self.error_codes[key]["available_resource"] -= resource
            if (self.error_codes[key]["available_resource"] <= 0) and (self.state == 0):  # self.status == 0 для учета 1-ой ошибки
                self.state = key

        self.available_resource -= resource

        if self.state:
            raise ATM_DEVICE_ERROR(self.alias, self.state, self.error_codes[self.state])

        return

    def apply_action(self, action):
        """
        Попытка сброса ошибок, траф будет считаться в банкомате (не в узле)
        :return:
        """
        if self.state != 0:
            prob, resource_percentage = self.error_codes[self.state].get(action, (0, 0))
            if prob > np.random.uniform(0, 1):
                # успешное применение действия
                self.logger.debug("{1}: {0} success".format(action, self.state))
                self.__repair(resource_percentage)
            else:
                # не прокатило
                self.logger.debug("{1}: {0} fault".format(action, self.state))
        return self.state

    def __repair(self, resource_percentage):
        if resource_percentage > 0:
            self.error_codes[self.state]["available_resource"] = self.error_codes[self.state]["resource"] * resource_percentage         # Восстанавливаем ресурс узла
            self.state = 0
            self.update_resource()
        return