from .device import DEVICE, ATM_DEVICE_ERROR
import logging

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(name)s [%(levelname)s]: %(message)s')


class ATM:
    BAD_ACTION_PENALTY = 1
    def __init__(self, atm_config, service_config):
        self.status = {
            "code": 0,
            "desc": None,
            "error": None
        }
        self.devices = {}
        self.service = service_config
        self.penalty = 0

        for dev_cfg in atm_config["devices"]:
            self.devices[dev_cfg["alias"]] = DEVICE(dev_cfg)

        self.logger = logging.getLogger('ATM')
        self.logger.debug("Initialized")
        return

    def make_transaction(self, volume=1):
        """
        Проведение транзакции
        проверить доступный ресурс по устройствам
        :return:
        """
        if self.status["code"] == 0:
            resource = min([self.devices[key].available_resource for key, value in self.devices.items()])
            if volume == 0:
                volume = resource
            else:
                volume = min(volume, resource)

            for key, value in self.devices.items():
                try:
                    self.devices[key].utilize_resource(volume)
                except ATM_DEVICE_ERROR as e:
                    self.status["code"] = e.code
                    self.status["desc"] = e.alias
                    self.status["error"] = e
        return self.status["code"]

    def apply_action(self, action):
        """
        :return:
        """
        penalty = 0
        if self.status["code"] != 0:

            penalty = self.service.get(action, self.BAD_ACTION_PENALTY)
            self.penalty -= penalty

            state = self.devices[self.status["desc"]].apply_action(action)
            if state == 0:
                self.status["code"] = 0
                self.status["desc"] = None
                self.status["error"] = None

        print(self.penalty)
        return

    def get_state(self):
        print("|", end="")

        for key, value in self.devices.items():
            print(" {0:>4} |".format(key), end="")
        print()
        print("|", end="")
        for key, value in self.devices.items():
            print(" {0:>4} |".format(self.devices[key].state), end="")
        print()


